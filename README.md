# Stargazers

Questa applicazione Android permette di cercare e vedere la lista degli utenti che hanno messo la "stella" a un repository GitHub.  
L'applicazione è compatibile con versioni di Android >= Lollipop.


## Istruzioni per l'esecuzione
Clonare repository BitBucket: git clone https://mattiapini@bitbucket.org/mattiapini/stargazers.git  
Tramite Android Studio importare il progetto (non aprire), il quale verificherà se tutte le dipendenze sono installate per la compilazione  
Installare l'applicazione su emulatore o su dispositivo reale direttamente da Android Studio  
Eseguire l'applicazione