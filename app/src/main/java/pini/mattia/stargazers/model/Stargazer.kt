package pini.mattia.stargazers.model

import android.os.Parcel
import android.os.Parcelable

data class Stargazer(val login: String?, val avatar_url: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(login)
        parcel.writeString(avatar_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Stargazer> {
        override fun createFromParcel(parcel: Parcel): Stargazer {
            return Stargazer(parcel)
        }

        override fun newArray(size: Int): Array<Stargazer?> {
            return arrayOfNulls(size)
        }
    }

}