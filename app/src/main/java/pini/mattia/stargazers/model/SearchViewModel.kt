package pini.mattia.stargazers.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import pini.mattia.stargazers.GitHubRequests
import kotlin.coroutines.CoroutineContext

class SearchViewModel :ViewModel(),CoroutineScope{

    var stargazers = MutableLiveData<List<Stargazer>>()
    private set
    var fetching = MutableLiveData<Boolean>()
    private set

    lateinit var lastRequestOwner : String
    lateinit var lastRequestRepository :String


    override val coroutineContext: CoroutineContext = Dispatchers.IO + SupervisorJob()


    fun fetchStargazersFromGithub(owner:String,repository:String) {
        fetching.value = true
        lastRequestOwner = owner
        lastRequestRepository = repository

        this.launch {
            val result = GitHubRequests.getStargazers(owner, repository)
            withContext(Dispatchers.Main) {
                stargazers.value = result
                fetching.value = false

            }

        }
    }
}