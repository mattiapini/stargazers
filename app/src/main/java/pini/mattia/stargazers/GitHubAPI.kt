package pini.mattia.stargazers

import pini.mattia.stargazers.model.Stargazer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubAPI {

    @GET("/repos/{owner}/{repository}/stargazers")
    suspend fun getStargazers(@Path("owner") owner:String, @Path("repository") repository:String) : Response<List<Stargazer>>

}