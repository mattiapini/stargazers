package pini.mattia.stargazers.ui

import android.content.Context
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import pini.mattia.stargazers.R
import pini.mattia.stargazers.model.SearchViewModel
import pini.mattia.stargazers.model.Stargazer


class MainActivity : AppCompatActivity(),
    HomeFragment.OnHomeFragmentListener {

    private lateinit var navController: NavController
    private lateinit var model: SearchViewModel

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        navController.navigateUp()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        val host = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return

        navController = host.navController

        model = run {
            ViewModelProviders.of(this)[SearchViewModel::class.java]
        }

        model.stargazers.observe(this,Observer<List<Stargazer>>{
            if (it != null) {
                val action = HomeFragmentDirections.postResult(model.lastRequestOwner, model.lastRequestRepository)
                navController.navigate(action)
            }else {
                AlertDialog.Builder(this@MainActivity)
                    .setTitle("Uhmmm...")
                    .setMessage("Repository not found... Did you insert correct information?")
                    .create()
                    .show()
            }
        })

    }

    override fun onSearchPressed(owner: String?, repository: String?, progressButton: Button) {

        if (isInternetConnected()) {
            model.fetchStargazersFromGithub(owner!!, repository!!)
        }else {
            //the user is not connected to internet, alert him
            AlertDialog.Builder(this)
                .setTitle("Whoops")
                .setMessage("Internet connection is not activated, please activate it and search again!")
                .setPositiveButton("Dismiss",null)
                .create()
                .show()
        }
    }

    @Suppress("DEPRECATION")
    private fun isInternetConnected() : Boolean{
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}
