package pini.mattia.stargazers.ui


import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_search_result.*
import pini.mattia.stargazers.R
import pini.mattia.stargazers.model.SearchViewModel
import pini.mattia.stargazers.model.Stargazer


/**
 * This fragment displays the informations about the searched repository and the list of stargazers
 */
class SearchResultFragment : Fragment() {

    private lateinit var model: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar!!.setDisplayHomeAsUpEnabled(true)

        val rvStargazerAdapter = SearchRecycleViewAdapter()
        rvStargazers.layoutManager = LinearLayoutManager(activity)
        rvStargazers.adapter = rvStargazerAdapter
        rvStargazers.setHasFixedSize(true)

        model = activity?.run {
            ViewModelProviders.of(this)[SearchViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        //grub navigation arguments
        txtRepository.text = arguments!!["repoName"] as String
        txtOwner.text = arguments!!["repoOwner"] as String

        model.stargazers.observe(this, Observer<List<Stargazer>>{
            rvStargazerAdapter.setStargazers(it)
            if (it.isEmpty()) {
                //if there aren't stargazers, print an alert dialog
                AlertDialog.Builder(context!!)
                    .setTitle("Ops")
                    .setMessage("It seems that this repository doesn't have any stargazer... Too bad...")
                    .setPositiveButton("Go Back"){_,_ ->  findNavController().navigateUp()}
                    .create()
                    .show()
            }
        })
    }

}
