package pini.mattia.stargazers.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pini.mattia.stargazers.R
import pini.mattia.stargazers.model.Stargazer


class SearchRecycleViewAdapter : RecyclerView.Adapter<SearchRecycleViewAdapter.ViewHolder>() {

    private lateinit var stargazers: List<Stargazer>

    fun setStargazers(stargazers: List<Stargazer>){
        this.stargazers = stargazers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //inflate the RecycleView item layout
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.search_result_rv_item,parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = stargazers.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get().load(stargazers[position].avatar_url).into(holder.imvOwner)
        holder.txtStargazer.text = stargazers[position].login
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imvOwner: ImageView = itemView.findViewById(R.id.imvImageStargazer)
        val txtStargazer: TextView = itemView.findViewById(R.id.txtNameStargazer)
    }
}