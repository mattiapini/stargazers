package pini.mattia.stargazers.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_home.*
import pini.mattia.stargazers.R
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import pini.mattia.stargazers.model.SearchViewModel


/**
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnHomeFragmentListener] interface
 * to handle interaction events.
 */
class HomeFragment : Fragment() {

    private var listener: OnHomeFragmentListener? = null
    private lateinit var model: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this)[SearchViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set the toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar2)

        edtRepository.setOnEditorActionListener{_,actionId,_ ->
            var result = false
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                checkFieldsAndGo()
                result = true
            }
            result
        }

        btnSearch.setOnClickListener {
            checkFieldsAndGo()
        }
        bindProgressButton(btnSearch)

        model.fetching.observe(this, Observer<Boolean>{fetchingData ->
            if (fetchingData){
                btnSearch.showProgress {
                    buttonTextRes = R.string.search_string
                    progressColor = Color.WHITE
                }
            }else{
                btnSearch.hideProgress(R.string.search_string)
            }
        })
    }

    /**
     * Check if all fields are filled and call listener interface
     */
    private fun checkFieldsAndGo(){
        val allFieldsFilled = (checkFieldAndError(edtOwnerLayout) and checkFieldAndError(edtRepositoryLayout))
        if(allFieldsFilled){
            closeKeyboard()
            listener?.onSearchPressed(edtOwner.text.toString(),edtRepository.text.toString(),btnSearch)
        }
    }

    private fun closeKeyboard() {
        val view = activity!!.currentFocus
        if (view != null) {
            val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnHomeFragmentListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnHomeFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * A function that checks if a field is filled and set an error otherwise
     * @param textInputLayoutToCheck Field to check
     * @return true if filled, false otherwise
     */

    private fun checkFieldAndError(textInputLayoutToCheck: TextInputLayout): Boolean{
        var filled = true
        if(textInputLayoutToCheck.editText!!.text!!.isEmpty()){
            textInputLayoutToCheck.error = resources.getString(R.string.not_filled_edittext_hint)
            filled = false
        }else{
            textInputLayoutToCheck.error = null
        }

        return filled
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     */
    interface OnHomeFragmentListener {

        fun onSearchPressed(owner:String?, repository:String?, progressButton: Button)
    }


}
