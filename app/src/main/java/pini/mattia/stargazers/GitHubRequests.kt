package pini.mattia.stargazers

import pini.mattia.stargazers.model.Stargazer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object GitHubRequests {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(GitHubAPI::class.java)

    suspend fun getStargazers(owner:String, repository:String) : List<Stargazer>?{
        return retrofit.getStargazers(owner,repository).body()
    }

}